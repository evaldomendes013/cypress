/// <reference types="Cypress"/>


describe('pagina inicial', () => {
    beforeEach(function () {
        cy.visit('/');
        cy.url().should('eq', 'http://localhost:4202/#/home')

    })

    it('Verifica o título da página', () => {
        cy.title().should('eq', 'FPFTech')

    })

    it('Verifica a URL da página', () => {
        cy.title().should('eq', 'FPFTech')

    })


    it('Cadastra um tipo de estante válido', () => {
        cy.get('#main_modules').click()
        cy.xpath('//div[@role="menu"]//button[contains(. ,"Cadastros básicos")]').click()
        cy.get('#menu-rack-type').click()
        cy.get('button[style="margin-right: 4px;"]').click()
        cy.get('input[formcontrolname="description"]').type('teste', {delay: 0})
        cy.get('input[formcontrolname="small_floor_quantity"]').type('1')
        cy.get('input[formcontrolname="small_floor_roll_quantity"]').type('2')
        cy.get('input[formcontrolname="big_floor_quantity"]').type('3')
        cy.get('input[formcontrolname="big_floor_roll_quantity"]').type('4')
        cy.get('button[type="submit"]').click()
        cy.get('.toast-message').should('have.text', ' Salvo com sucesso ')
    })

    it('Cadastra um tipo de estante com descrição já utilizada', () => {
        cy.get('#main_modules').click()
        cy.xpath('//div[@role="menu"]//button[contains(. ,"Cadastros básicos")]').click()
        cy.get('#menu-rack-type').click()
        cy.get('button[style="margin-right: 4px;"]').click()
        cy.get('input[formcontrolname="description"]').type('teste', {delay: 0})
        cy.get('input[formcontrolname="small_floor_quantity"]').type('1')
        cy.get('input[formcontrolname="small_floor_roll_quantity"]').type('2')
        cy.get('input[formcontrolname="big_floor_quantity"]').type('3')
        cy.get('input[formcontrolname="big_floor_roll_quantity"]').type('4')
        cy.get('button[type="submit"]').click()
        cy.get('.toast-message').should('have.text', ' Estante com essa descrição já existe. Por favor, tente novamente. ')
    })



})
