const { defineConfig } = require('cypress')

module.exports = defineConfig({
  e2e: {
    baseUrl: 'http://localhost:4202',
  },
  fixturesFolder: false,
  video: false,
})
